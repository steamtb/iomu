#pragma once
/* iomu - high fidelity i/o emulation for qemu 
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * QEMU emulated devices interact with guest drivers and host i/o through 
 * memory mapping. These mappings take on various forms such as virtio or 
 * emulation of an actual physical bus protocol such as PCI or ISA. One
 * consequence of this approach is the i/o performance of devices is an
 * artifact of the host machine performance. The iomu system decouples guest
 * performance from host performance buy pushing i/o through a chain of rings  
 * of type IOMURing. These rings are parameterized with performance 
 * specification and do thier best to honor that specification. The rings can
 * be chained together in a linked list to create i/o topologies, such as a 
 * network i/o path that a) goes across a PCI bus and b) is sent out a PHY. 
 * Both (a) and (b) have independent performance characteristics that can be
 * represented in sum by a chain of two rings.
 *
 * */

#include <stdlib.h>
#include <stdint.h>
#include <sys/uio.h>
#include <math.h>

# define likely(x)  __builtin_expect(!!(x), 1)
# define unlikely(x)  __builtin_expect(!!(x), 0)

static const uint64_t ER_BASE = 2ull << 63;

typedef struct IOMURing    IOMURing;
typedef struct IOMUParam   IOMUParam;
typedef struct IOMUSpec    IOMUSpec;

typedef struct iomu_internal iomu_internal;
typedef struct iomu_element iomu_element;

/* emulation parameters that influence how emulation is performed */
struct IOMUParam {
  /* 
   * Time resolution in nanoseconds. This means that within each time
   * interval @dt, (@IOMUSpec.throughput*1e-9) * @dt will pass through
   * the ring. iovecs that are larger than the resolution window
   * W := (@IOMUSpec.throughput*1e-9) will be dynamically subdivided into
   * smaller iovs that can be transmitted within the window without violating
   * the bandwidth performance specification. 
   */
  uint64_t dt;    

  /*
   * Data resolution in bytes. Each iovec that gets pushed into the ring that
   * is larger than @chunk will get split up into smaller iovs. This value
   * is computed by the implementation, setting it manually will result in
   * undefined behavior.
   */
  uint64_t chunk;

  /*
   * data resolution in chunks per time frame. the implementation will send
   * @dd @chunk sized transmissions chunks every @dt nanoseconds. this value
   * combined with @IOMUSpec.throughput is used to compute the value of chunk
   */
  uint64_t dd;
};

/* performance specification of the i/o path represented by a given IOMURing */
struct IOMUSpec {
  uint64_t throughput; /* bytes per second */
  uint64_t latency;    /* nanoseconds */
  uint64_t ber;        /* bit error rate (bit errors per 2^64 bits) */
  uint64_t blr;        /* bit loss rate (bits lost per 2^64 bits) */
};

struct IOMURing {
  IOMURing  *next;
  IOMUParam  params;
  IOMUSpec   spec;
  iomu_internal *_;
  void *harg; //handler argument: opaque pointer passed to handler callback
  void (*handler)(struct iovec, void *opaque);
};

IOMURing* iomu_create (IOMUParam params, IOMUSpec spec, 
    void(*handler)(struct iovec, void *opaque));

void iomu_run (IOMURing *r);
void iomu_destroy (IOMURing **r);
void iomu_push (IOMURing *r, struct iovec iov);
size_t iomu_len (IOMURing *r);
size_t iomu_sent (IOMURing *r);
size_t iomu_pushed (IOMURing *r);

static inline uint64_t iomu_error_rate(double value, double magnitude)
{
  long double numerator = (long double)ER_BASE;
  long double divisor = pow(10.0, magnitude);

  return numerator / divisor * value;
}

static inline uint64_t iomu_milliseconds(uint64_t ns)
{
  return ns * 1e-6;
}

static inline uint64_t iomu_mBps(uint64_t x)
{
  return x * 1e6;
}

static inline uint64_t iomu_gBps(uint64_t x)
{
  return x * 1e9;
}

static inline uint64_t iomu_gbps(uint64_t x)
{
  return (x * 1e9)/8;
}

