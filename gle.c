/******************************************************************************
 *
 * general link emulator
 *
 *****************************************************************************/

#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>
#include "iomu.h"

int parseIntArg(char *value);
void usage();
void run(IOMUSpec spec, char *ifx0, char *ifx1);
static void gle_handler(struct iovec iov, void *opaque);

int main(int argc, char **argv) {

  char *throughput = NULL,
       *latency = NULL,
       *ber = NULL,
       *blr = NULL,
       *ifx0 = NULL,
       *ifx1 = NULL;

  while(1) {

    static struct option long_options[] = {
      {"help", no_argument, 0, 'h'},
      {"throughput", required_argument, 0, 't'},
      {"latency",    required_argument, 0, 'd'},
      {"ber",        required_argument, 0, 'e'},
      {"blr",        required_argument, 0, 'l'},
    };

    int option_index = 0;
    int c = getopt_long(argc, argv, "ht:d:e:l:", long_options, &option_index);

    if (c == -1) break;

    switch (c) {
      case 'h':
        usage();
        exit(1);
      case 't':
        throughput = malloc(strlen(optarg));
        strcpy(throughput, optarg);
        break;
      case 'd':
        latency = malloc(strlen(optarg));
        strcpy(latency, optarg);
        break;
      case 'e':
        ber = malloc(strlen(optarg));
        strcpy(ber, optarg);
        break;
      case 'l':
        blr = malloc(strlen(optarg));
        strcpy(blr, optarg);
        break;
    }

  }

  if (optind+2 > argc) {
    usage();
    exit(1);
  }

  ifx0 = malloc(strlen(argv[optind+1]));
  ifx1 = malloc(strlen(argv[optind+2]));
  strcpy(ifx0, argv[optind+1]);
  strcpy(ifx1, argv[optind+2]);

  IOMUSpec spec;

  if(!throughput) {
    fprintf(stderr, "throughput not provided, setting to 1gbps\n");
    spec.throughput = iomu_gbps(1);
  }
  else {
    int val = parseIntArg(throughput);
    spec.throughput = iomu_gbps(val);
  }

  if(!latency) {
    fprintf(stderr, "latency not provided, setting to 0\n");
    spec.latency = 0;
  }
  else {
    int val = parseIntArg(latency);
    spec.latency = iomu_milliseconds(val);
  }

  if(!ber) {
    fprintf(stderr, "ber not provided, setting to 0\n");
    spec.ber = 0;
  }
  else {
    spec.ber = parseIntArg(ber);
  }

  if(!blr) {
    fprintf(stderr, "blr not provided, setting to 0\n");
    spec.blr = 0;
  }
  else {
    spec.blr = parseIntArg(blr);
  }

  run(spec, ifx0, ifx1);

}

void run(IOMUSpec spec, char *ifx0, char *ifx1) {

  IOMUParam params = {
    .dt = 1e6, /* 1 ms time window */
    .dd = 100, /* 100 packets per time window */
  };
  IOMURing *ior = iomu_create(params, spec, gle_handler);
  ior->harg = ifx1;


  // read packets from ifx0 here using DPDK, ifx0 and ifx1 are likely
  // tap interfaces, but really any type of interface should be supported

}

static void gle_handler(struct iovec iov, void *opaque) {

  // write paackets to ifx1 here using DPDK
  char *ifx1 = (char*)opaque;

}

int parseIntArg(char *str) {
    long val = strtol(str, NULL, 10);
    if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
        || (errno != 0 && val == 0)) {
      perror("invalid throughput");
      exit(EXIT_FAILURE);
    }
    return val;
}

void usage() {
  printf("generic link emulator\n"
      "  gle [options] ifx0 ifx1\n"
      "    --throughput n        gbps\n"
      "    --latency n           milliseconds\n"
      "    --ber n               bit errors per 10^12 bits\n"
      "    --blr n               bit losses per 10^12 bits\n"
  );
}
