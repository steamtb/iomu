// virtio-nic initialization
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// tx-queue setup
//---------------

virtio_net_device_realize(DeviceState *dev, ...)
  n = VIRTIO_NET(dev)
  for(i=0; i<n->max_queues; i++) {
    virtio_net_add_queue(n, i)
      vdev = VIRTIO_DEVICE(n)

      // this is the primary queue through which the guest driver and the 
      // virtual nic communitate. the queue is actually two queues, one called
      // AVAILABLE and one called USED. When data is available for the nic to 
      // consume, the driver pushes io descriptors into the AVAILABLE queue.
      // when the NIC has processed them, it puts the associated io descriptor
      // in the USED queue (which can be read by the driver if so desired)

      // this queue is also used to signal the bottom half queue. once a
      // transmission has completed, the virtio_net_handle_tx_bh will
      // schedule the nic's bottom half to be run again via the tx_bh queue
      // below
      n->vqs[i].tx_vq = virtio_add_queue(vdev, n->net_conf.tx_queue_size,
                                         virtio_net_handle_tx_bh)

      // creates a new bottom half an insertes into the front of of the QEMU
      // global bottom half linked list. The bottom half is defined by the
      // virtio_net_tx_bh handler. A 'bottom-half' is an I/O device
      // implementation that can potentially run long running operations.
      // Bottom halves can create threads for really long running operations.
      n->vqs[i].tx_bh = qemu_bh_new(virtio_net_tx_bh, &n_.vqs[i])
  }

  n->nic = qemu_new_nic(
    &net_virtio_info,  // see below
    ...
  )


// 'starting' the NIC 
//-------------------

// when the link status goes changes, the bottom half will be scheduled
NetClientInfo net_virtio_info = {
  // ...
  .link_status_changed = virtio_net_set_link_status
}

virtio_net_set_link_status(nc)
  n = qemu_get_nic_opaque(nc)
  vdev = VIRTIO_DEVICE(n)
  virtio_net_set_status(vdev, ...)
    n = VIRTIO_NET(vdev)
    for(i=0; i<n->max_queues; i++) {
      q = &n->vqs[i]
      qemu_bh_schedule(q->tx_bh) //<----- bottom half scheduling
    }



// TX
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// tx entry point
//---------------

// this function is called asynchronously by the bottom half scheduler
virtio_net_tx_bh(void *opaque)
  VirtIONetQueue *q = opaque

  // send the pending packets out the font door
  virtio_net_flush_tx(q)
    // calls provided callback when send is complete, in this case to enqueue
    // and element into the tx_vq queue, which is a signal to reschedule the 
    // bottom half
    qemu_sendv_packet_async(..., virtio_net_tx_complete)  // see NIC <--> wire below


// details on virtio_net_flush_tx
//-------------------------------
//   note that virtqueues are actually two queues
//     - AVAILABLE queue: for data from guest driver to virtual nic
//     - USED queue: for data from virtual nic to guest driver
//   see the original virtio paper by Rusty Russell for more details

virtio_net_flush_tx(q)
  for(;;) {

    // get an element from the *AVAILABLE* queue
    elem = virtqueue_pop(q->tx_vq, ...) // <---------   see RAM <--> NIC below

    // send the packet out the front door
    ret = qemu_sendv_packet_async(...)

  }

  // push the transmitted element into the *USED* queue and notify listeners of
  // that queue
  virtqueue_push(q->tx_vq, elem, 0)
  virtio_notify(vdev, q->tx_vq)



// RAM <--> NIC
//-------------

// the emulation of memory moving from RAM to the NIC happens when the the virtual
// NIC pops an item from it's virtqueue. It goes like this

virtqueue_pop(VirtQueue *vq, ...)
  VirtIODevice *vdev = vq->vdev
  VirtQueueElement *elem   // the result of the pop
  iovec iov[VIRTQUEUE_MAX_SIZE] // the iovs that go in the element

  // read all the available descriptors in the queue
  do {

    // map the data referenced by the current descriptor into a local io-vector
    virtqueue_map_desc(vdev, iov, idx, ..., desc.addr, desc.len)
      
      // DMA read loop until all descriptor data has been read
      sz = desc.len
      while(sz) {

        // use DMA to read the descriptor data from system memory into the
        // local iovec
        iov[idx].iov_base = dma_memory_map(
          vdev->dma_as, 
          physical_address, 
          &len,
          is_write ? DMA_DIR_FROM_DEV : DMA_DIR_TO_DEV
        )
        sz -= len
        idx++

      }

    // grab the next descriptor
    rc = virtqueue_read_next_desc(..., &desc, ...)
  } while (rc == VIRTQUEUE_READ_DESC_MORE)


// dma memory map details
dma_memory_map(AddressSpace *as, dma_addr_t addr, dma_addr_t *len, DMADirection dir)
  hwaddr xlen = *len
  void *p = address_space_map(as, addr, &xlen, dir == DMA_DIR_FROM_DEV)
    // return host pointer to guest's ram
    return qemu_ram_ptr_length(...)
  *len = xlen
  return p
   
////
//// At this point, no actual memory has been moved around, we have just
//// computed mappings between different address spaces (guest memory, host
//// memory, and device memory). Actual use of the memory pointed to by the
//// mapped addresses comes next
////


// NIC <--> wire
//--------------

qemu_sendv_packet_async(NetClientState *sender,
                        iovec *iov, int iovcnt, NetPacketSent sent_cp)

  queue = sender->peer->incomming_queue
  qemu_set_queue_send_iov(
    queue,
    sender,
    QEMU_NET_PACKET_FLAG_NONE,
    iov, iovcnt,
    sent_cb
  )
    if(canDeliverNow) {

      ret = qemu_net_queue_deliver_iov(queue, sender, flags, iov, iovcnt)

        quene->delivering = 1
        ret = queue->deliver(sender, flags, iov, iovcnt, queue->opaque)
        queue->delivering = 0
        return ret

      qemu_net_queue_flush(queue)

      return ret
    }
    else {
      qemu_net_queue_append_iov(queue, sender, flags, iov, iovcnt, sent_cb)
        packet = malloc(packet)
        copy_iov_data(packet->data, iov)
        QTAILQ_INSERT_TAIL(&queue->packets, packet, entry)
      return
    }


// QemuNetClient queue setup
//--------------------------

// establish queue delivery mechanism
qemu_net_client_setup(NetClientState *nc, ....)
  nc->incomming_queue = qemu_new_net_queue(qemu_deliver_packet_iov, nc)
    NetQueue *queue
    queue->deliver = qemu_deliver_packet_iov

// a look inside delivery mechanisim
qemu_deliver_packet_iov(iovec *iov, int iovcnt, void *opaque)
  NetClientState *nc = opaque
  if(nc->info->receive_iov && !(flats & QEMU_NET_PACKET_FLAG_RAW)) {
    ret = nc->info->receive_iov(nc, iov, iovcnt)
  }
  else {
    // other things not looking into here...
  }
  return ret

// the receive_iov function depends on what type of netdev is backing the 
// emulated network device, recall that network device specification looks
// something like this in qemu
//
//   qemu-system-x86_64
//     -netdev tap,id=net0,ifname=tapX
//     -device virtio-net-pci,netdev=net0,id=net0,mac=ab:cd:ef:00:11:22
//
// this example uses a 'tap' device, but there are others. The code below
// also fallows the tap device path

// This is where the recieve_iov function gets bound in net/tap.c
NetClientInfo net_tap_info = {
  ...
  .receive_iov = tap_reveive_iov
  ...
}

tap_receive_iov(..., iovec iov, int iovcnt)
  tap_write_packet(s, iov, iovcnt)
    do {
      len = writev(s->fd, iov, iovcnt)
    } while (len == -1 && errno == EINTR)
