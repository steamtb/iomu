# iomu
High fidelity inter-component i/o emulation for QEMU.

This repository contains an i/o channel that can be parameterized with a set of performance properties and placed between communicating QEMU components. At this time it's designed specifically to work with virtio components. An example for virtio-net is depicted below, where the iomu channel is in teal.

![](doc/img/virtio-example.png)

## Example iomu setup
```c
IOMUParam params = { .dt = 1e6, .dd = 100 };
IOMUSpec spec = {
	.throughput = iomu_mBps(985),  // pcie3 single lane
	.latency = iomu_ns(3),
	.ber = iomu_error_rate(1, 16), // 1 error per 1e16 bits
	.blr = iomu_error_rate(1, 18)  // 1 loss per 1e18 bits
};

iomu_create(
	params, 
	spec, 
	do_virtqueue_map_desc);
```

The channel is a simple ring buffer mechanism that creates a background thread to perform I/O between several consumers and producers. Producers can push `iovec`s into the ring. When an `iovec` is scheduled to leave the ring, a user provided callback is called with the `iovec` as an argument it is logically removed from the ring. Through the callback mechanism, rings can also be chained together to get composite performance behavior.

## Supported Parameters

At this time the following parameters are either working or will be soon.

- throughput
- latency
- bit error rate
- bit loss rate

