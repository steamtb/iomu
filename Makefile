CFLAGS += $(shell pkgconf --cflags glib-2.0)
CFLAGS += -g -Wall -Wextra -O2

LDFLAGS += $(shell pkgconf --libs glib-2.0)
LDFLAGS += -pthread -lm

.PHONY: all
all: tests gle

tests: iomu.h iomu.c test.c Makefile
	gcc $(CFLAGS) $(LDFLAGS) test.c iomu.c -o $@

gle: gle.c iomu.h iomu.c Makefile
	gcc $(CFLAGS) $(LDFLAGS) gle.c iomu.c -o $@

.PHONY: check
check: tests
	avocado run --loaders glib -- ./tests

.PHONY: clean
clean:
	rm -f *.o
	rm -f tests
	rm -f *.csv
	rm -f gle
