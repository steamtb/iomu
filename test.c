#include "iomu.h"
#include <stdio.h>
#include <string.h>
#include <sys/queue.h>
#include <glib.h>
#include <time.h>
#include <unistd.h>

#define UNUSED(x) (void)x

#define DATA_SIZE 512
#define CHUNK_SIZE 256

/* wire data structure */
typedef struct Msg {
  double t;
  uint32_t data[DATA_SIZE];
} __attribute__((packed)) Msg;

/* memory pointers used for tests */
Msg *src = NULL;
Msg *dst = NULL;
double *arr = NULL;
const size_t N = 10e6/sizeof(Msg);
// because the data gets broken up dynamically inside the ring, it's
// not terribly obvious what this number should be, so just pad it up
const size_t M = N*sizeof(Msg)/CHUNK_SIZE + 100000;

size_t arr_sz = 0;
double arr0=0, arr1=0;

/* tests */
static void test_init();
static void test_baseline();
static void test_push();

/* helpers */
static void dump_data(const Msg *data, size_t n, const char *filename);
static void dump_data2(const double *data, size_t n, const char *filename);
static void test_handler1(struct iovec iov, void *opaque);

int main(int argc, char **argv) 
{
  g_test_init(&argc, &argv, NULL);
  g_test_add_func("/iomu/init", test_init);
  g_test_add_func("/iomu/baseline", test_baseline);
  g_test_add_func("/iomu/push", test_push);

  return g_test_run();
}

/* tests ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/*
 * basic unit tests for data structure initialization
 */
static void test_init() 
{
  IOMUParam params = {
    .dt = 1e6, /*1 millisecond resolution */
    .dd = 100 /* send 100 chunks per frame */
  };
  IOMUSpec spec = {
    .throughput = iomu_mBps(20),
    .latency = 3,
    .ber = iomu_error_rate(1, 16),
    .blr = iomu_error_rate(1, 18)
  };
  IOMURing *ioq = iomu_create(params, spec, NULL);

  g_assert_nonnull(ioq);

  g_assert_cmpint(ioq->params.dt, ==, params.dt);
  g_assert_cmpint(ioq->params.dd, ==, params.dd);

  g_assert_cmpint(ioq->spec.throughput, ==, spec.throughput);
  g_assert_cmpint(ioq->spec.latency, ==, spec.latency);
  g_assert_cmpint(ioq->spec.ber, ==, spec.ber);
  g_assert_cmpint(ioq->spec.blr, ==, spec.blr);

  g_assert_null(ioq->next);
  g_assert_null(ioq->handler);

  iomu_destroy(&ioq);
}

/*
 * write the source data to memory as a baseline for *ideal* intermessage timing
 * 
 * artifacts:
 *  + baseline.csv - intermessage timings
 */
static void test_baseline() 
{
  free(src);
  free(dst);
  src = malloc(sizeof(Msg)*N);
  dst = malloc(sizeof(Msg)*N);

  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
  double t0 = ts.tv_sec + (ts.tv_nsec * 1.0e-9);
  double t1 = t0;

  for(size_t i=0; i<N; i++) {
    memcpy(dst[i].data, src[i].data, sizeof(src[i].data));
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    t1 = ts.tv_sec + (ts.tv_nsec * 1.0e-9);
    src[i].t = t1 - t0;
    t0 = t1;
  }
  dump_data(src, N, "baseline.csv");
}

/*
 * push packets through the ring with a handler that writes to memory
 * 
 * artifacts:
 *  + push.csv - intermessage send timings
 *  + arr.csv  - intermessage recv timings
 */
static void test_push() 
{
  IOMUParam params = {
    .dt = 1e6, /* 1 ms time window */
    .dd = 100, /* 100 packets per time window */
  };
  IOMUSpec spec = {
    .throughput = iomu_mBps(100),   /* 100 mBps ring */
    .latency = 3,                   /* delay not implemented yet ... */
    .ber = iomu_error_rate(1, 16),  /* bit error rate not implemented yet ... */
    .blr = iomu_error_rate(1, 18)   /* bit loss rate not implemented yet ... */
  };
  IOMURing *ioq = iomu_create(params, spec, test_handler1);

  /* launch the ring (runs in an async background thread) */
  iomu_run(ioq);

  /* prepare the data */
  free(src);
  free(dst);
  src = malloc(sizeof(Msg)*N);
  dst = malloc(sizeof(Msg)*N);
  arr = malloc(sizeof(double)*M);
  for(size_t i=0; i<N; i++) {
    for(size_t j=0; j<DATA_SIZE; j++) {
      src[i].data[j] = j;
    };
  }
  printf("sending %zu bytes\n", sizeof(Msg)*N);

  /* setup benchmark time and data counters*/
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
  double t0 = ts.tv_sec + (ts.tv_nsec * 1.0e-9);
  double start = t0;
  double t1 = t0;
  size_t inserted = 0;

  /* send data through the ring */
  for(size_t i=0; i<N; i++) {
    struct iovec iov = {
      .iov_base = &src[i],
      .iov_len = sizeof(Msg)
    };
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    t1 = ts.tv_sec + (ts.tv_nsec * 1.0e-9);
    src[i].t = t1 - t0;
    iomu_push(ioq, iov);
    t0 = t1;
    inserted += iov.iov_len;
  }

  /* wait for finish */
  while(iomu_len(ioq)) {}

  /* final time keeping and report measurements */
  clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
  double finish = ts.tv_sec + (ts.tv_nsec * 1.0e-9);
  printf("inserted: %zu\n", inserted);
  printf("duration: %.e\n", finish - start);
  printf("pushed: %zu\n", iomu_pushed(ioq));
  printf("sent: %zu\n", iomu_sent(ioq));

  /* make sure the data made it through the ring OK */
  for(size_t i=0; i<N; i++) {
    for(size_t j=0; j<DATA_SIZE; j++) {
      g_assert_cmpint(dst[i].data[j], ==, j);
    };
  }

  /* dump intermessage/interbuffer timing results */
  dump_data(dst, N, "push.csv");
  dump_data2(arr, arr_sz, "arr.csv");
}

/* helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/* 
 * this test handler records inter-iov timing and copies the 'recieved' buffers
 * into memory, so we can check the integrity of the data after the test full
 * transmission through the ring has been completed.
 */
static void test_handler1(struct iovec iov, void *opaque)
{
  UNUSED(opaque);

  struct timespec ts;

  clock_gettime(CLOCK_MONOTONIC_RAW, &ts);

  if(unlikely(arr_sz == 0)) {
    arr0 = ts.tv_sec + (ts.tv_nsec * 1.0e-9);
  }
  else {
    arr1 = ts.tv_sec + (ts.tv_nsec * 1.0e-9);
    arr[arr_sz] = arr1 - arr0;
    arr0 = arr1;
  }


  g_assert_cmpint(arr_sz, <, M);

  static size_t i = 0;
  char *buf = (char*)dst;
  memcpy(&buf[i], iov.iov_base, iov.iov_len);
  i += iov.iov_len;
  arr_sz++;
}

static void dump_data(const Msg *data, size_t n, const char *filename)
{
  FILE *f = fopen(filename, "w");
  fprintf(f, "x,y\n");
  for(size_t i=0; i<n; i++) {
    fprintf(f, "%zu,%.*e\n", i, DECIMAL_DIG, data[i].t);
  }
}

static void dump_data2(const double *data, size_t n, const char *filename)
{
  FILE *f = fopen(filename, "w");
  fprintf(f, "x,y\n");
  for(size_t i=0; i<n; i++) {
    fprintf(f, "%zu,%.*e\n", i, DECIMAL_DIG, data[i]);
  }
}
